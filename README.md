# ArchLinux 如何配置出属于自己的Hyprland
朋友们好。本期专栏是Hyprland的配置指南。不喜勿喷。
### 1. 自定义配置前
本期教程属于修改配置教程，所以建议从[这里](https://www.bilibili.com/video/BV15T411W7E8/?spm_id_from=333.788.recommend_more_video.-1)先安装一下，适合新手，方便后续配置。
### 2. Hyprland HiDPI
如果您是高分屏用户，在安装上述配置后，打开软件非常模糊，需要安装几个HiDPI必要的包。如果有冲突，请直接替换。
```shell
paru -S xorg-xwayland-hidpi-xprop wlroots-hidpi-xprop-git hyprland-hidpi-xprop-git xorg-xrdb
```
然后进入~/.config/hypr，编辑hyprland.conf，插入或修改以下内容：
```
xwayland {
  force_zero_scaling = true
}

env = GDK_SCALE,2
env = XCURSOR_SIZE,24

exec-once = echo 'Xft.dpi:192' | xrdb -merge 
```
重启后，就可以看到不模糊了。
### 3. fcitx5 输入法
fcitx5在wayland上的配置和xorg的不一样。**有配置的需要删除~/.pam_environment和~/.xprofile**。删除后执行以下命令
```
sudo pacman -S --needed fcitx5 fcitx5-chinese-addons fcitx5-qt fcitx5-gtk fcitx5-qt5 fcitx5-lua fcitx5-configtool
```
安装后，在/etc/environment添加以下内容:
```
QT_IM_MODULE=fcitx5
SDL_IM_MODULE=fcitx5
XMODIFIERS=@im=fcitx5
```
在Hyprland中添加以下内容:
```
exec-once = fcitx5 -d --replace
```
重启后就能看到输入法了。
### 4. 快捷键
如果您不知道对应的快捷键，请查看~/.config/hypr/hyprland.conf 中的 *bind = xxxxxx*。

如果您要修改快捷键，请把除了$mainMod之外的所有按键大写，以及两个功能键间只有空格，没有逗号。

~~如果连终端的快捷键都不知道，请打开tty操作。~~

### 5. Waybar
![waybar](https://gitlab.com/l.r/HyprGuide/-/raw/main/Waybar.png?inline=false)
Waybar的配置位于~/.config/waybar。通常，您的Hyprland顶栏就是waybar。会一点的朋友可以修改目录下的style.css和config.jsonc。配置见仓库

### 6. 通知
如果您的Hyprland没用通知，可以使用mako。
```
paru -S mako
```
在~/.config/hypr/hyprland.conf中添加以下内容：
```
exec-once = mako
```
在~/.config/mako/config中添加以下内容：
```
layer=top
anchor=top-right
font=JetBrains Mono
background-color=#0f0f0f
text-color=#f0f0f0
width=350
margin=0,30,30,30
padding=10
border-size=2
border-color=#262626
progress-color=over #262626ff
border-radius=8
default-timeout=3000
group-by=summary

[grouped]
format=<b>%s</b>\n%b

[mode=do-not-disturb]
invisible=1

```
终端输入```makoctl reload```，重启一下，然后使用这条命令测试。
```
notify-send 'Hello world!' 'This is an example notification.' -u normal
```
### 7. Hyprland配置
> **致谢信息**<br>
> 以下内容[来自这篇文章](https://cascade.moe/posts/hyprland-configure/)修改而成，感谢作者。

您可以查看以下信息，有哪些需要的加进hyprland.conf中。
#### 显示器
显示器设置主要关于显示器的分辨率、位置与缩放比例。显示器设置的格式如下：
```
monitor = monitor_name, resolution, position, scale
```
你可以通过指令 hyprctl monitors 来列出所有可用的显示器。


#### 输入
这一部分主要与键盘、鼠标和触摸板的配置有关。通常来讲默认配置也能够正常使用。
```
input {
    kb_layout = us              # 键盘布局
    follow_mouse = 1            # 窗口焦点是否随光标移动
    touchpad {
        natural_scroll = no     # 触摸板自然滚动
    }
    sensitivity = 0             # 鼠标灵敏度
    accel_profile = flat        # 鼠标加速的配置方案, flat 为禁用鼠标加速
}
```

#### 总体设置
这一部分主要与窗口的布局与窗口边框的设置有关。
```
general {
    gaps_in = 6         # 窗口之间的间隙大小
    gaps_out = 12       # 窗口与显示器边缘的间隙大小
    border_size = 2     # 窗口边框的宽度
    col.active_border = rgba(cceeffbb)      # 活动窗口的边框颜色 
    col.inactive_border = rgba(595959aa)    # 非活动窗口的边框颜色
    layout = dwindle    # 窗口布局
}
```

#### 窗口装饰
这一部分主要和窗口的圆角、毛玻璃、投影等效果有关。
```
decoration {
    rounding = 12       # 圆角大小
    blur = yes          # 模糊效果是否启用
    blur_size = 5       # 模糊半径
    blur_passes = 1     # 模糊过滤次数
    blur_new_optimizations = on     # 模糊优化，通常保持打开
    drop_shadow = yes   # 窗口投影是否启用
    shadow_range = 4    # 投影大小
    shadow_render_power = 3     # 投影强度，不过我不太明白这是什么意思
    col.shadow = rgba(1a1a1aee)     # 投影颜色
    # 透明度
    active_opacity        =     0.87
    inactive_opacity      =     0.87
    fullscreen_opacity    =     0.87
}
```
#### 动画
这一部分主要和窗口的过渡动画有关。
```
animations {
    enabled = yes   # 是否启用动画
    bezier = myBezier, 0.05, 0.9, 0.1, 1.05     # 自定义的贝塞尔曲线
    animation = windowsMove, 1, 7, myBezier             # 窗口移动动画
    animation = windowsIn, 1, 3, default, popin 90%     # 窗口打开动画
    animation = windowsOut, 1, 3, default, popin 90%    # 窗口关闭动画
    animation = border, 1, 2, default       # 边框颜色动画
    animation = fade, 1, 3, default         # 窗口各种 Fade 效果（打开、关闭、阴影、活动与非活动窗口切换）的动画
    animation = workspaces, 1, 3, default   # 工作区切换动画
}
```
### 8. rofi
![rofi](https://gitlab.com/l.r/HyprGuide/-/raw/main/Rofi.png?inline=false)
rofi是一个搜索工具。如果您觉得现在的wofi够好看了，那就不用看下面的内容了。
rofi的配置见仓库。

---
### END
剩下的就没什么好说的了，祝大家使用愉快！
~~如果您都不想用了，可以直接用我的配置。~~